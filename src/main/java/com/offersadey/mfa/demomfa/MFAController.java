package com.offersadey.mfa.demomfa;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;

@EnableOAuth2Sso
@RestController
public class MFAController {

    @RequestMapping("/")
    String home(java.security.Principal user) {
        return "Logged as " + user.getName();
    }

}
