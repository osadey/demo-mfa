<h1>Muti-Factor Authentication Demo with OKTA integration</h1>
<p/>
Manage several factors:

- Mobile (Android/iPhone) Push notifications with Google Authenticator & Okta Verify mobile applications
- SMS Authentication
- ...


<h3>Pre-requisites<h3/>

- Create and activate a dev account (https://developer.okta.com)
- Login and add new Application
- Clic on the 'Web' button
- Go to the 'Signon panel' and clic on 'Add Rule' located on the 'Sign-on Policy' panel
- Choose 'Prompt for factor' on 'Every Sign-on' 
- Copy/paste values from console settings into your 'application.yml' (URL, OpenID Connect parameters)
- Switch to 'Classic UI' menu
- Go to the 'Security/Multifactor' menu and activate one of the Push Notification Factors

<h3>Build & Run the Demo<h3/>

Build the project:
- `mvn clean install`

Run the Demo:
- Install a mobile app from your store (Google Authenticator or Okta Verify)
- Run the SpringBoot Class named 'com.offersadey.mfa.demomfa.DemoMfaApplication'
- Open the following url on your browser: http://localhost:8080
- Login and enroll for MFA by scanning the QR Code on the screen
- Accept or Refuse login from your Mobile device





